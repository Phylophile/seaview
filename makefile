FLTK = ../../fltk/fltk-1.3
#PDF = ../../PDFlib-Lite-7.0.5/libraries
PDF = ../../PDFlibLite/libs/pdflib
X11 = /usr/X11R6
DEBUG = 
CXX = /Applications/Xcode.app/Contents/Developer/Toolchains/XcodeDefault.xctoolchain/usr/bin/clang++ 
CC = /Applications/Xcode.app/Contents/Developer/Toolchains/XcodeDefault.xctoolchain/usr/bin/clang 
CCW32 = /usr/local/cross4/bin/mingw32-g++

CFLAGS  =  -Dunix -DUSE_XFT  -U__APPLE__ -I$(PDF) -c $(OPT) -I$(FLTK) -I../../acnuc/socket/apic/trunk -Icsrc -I$(X11)/include $(DEBUG) -I.

CFLAGSW32  =  -c -O3  -I$(FLTK) -I$(PDF) -I../../acnuc/socket/apic/trunk  -Icsrc

#macro conditionnelle: pendant execution targets win32 CC a autre valeur
win32 : CXX = $(CCW32)  
win32 : CC = /usr/local/cross4/bin/mingw32-gcc  
win32 : CFLAGS =  $(CFLAGSW32)  -I../../../keep/cross/zlib-1.2.1  
win32 : PDFLIB = ../../PDFlibLite/libs/pdflib


OBJECTS = seaview.o custom.o use_mase_files.o regions.o load_seq.o align.o xfmatpt.o comlines.o \
 resource.o nexus.o viewasprots.o racnuc_fetch.o concatenate.o \
raa_acnuc.o parser.o md5.o zsockr.o misc_acnuc.o statistics.o \
trees.o treedraw.o addbootstrap.o least_squares_brl.o dnapars.o protpars.o lwl.o \
bionj.o phyml_util.o pseudoterminal.o unrooted.o svg.o pdf_or_ps.o threads.o phylip.o seq.o


seaview : $(OBJECTS)
	$(CXX) $(DEBUG) -o ../seaviewUnix/$@ $(OBJECTS) \
	     -L$(FLTK)/lib -lfltk13X11debug -lpng -lfltkjpeg \
             -L$(X11)/lib -lXft  -lfontconfig  -lX11 \
             -L$(PDF)/../pdflib-xcode -lPDFlib -lpdfJPEG -lpdfTIFF -lpdfPNG -lpdcore  -lz 

seaview.o : seaview.cxx
	$(CXX) $(CFLAGS) seaview.cxx

raa_acnuc.o : ../../acnuc/socket/apic/trunk/raa_acnuc.c
	$(CC) -c ../../acnuc/socket/apic/trunk/raa_acnuc.c
parser.o : ../../acnuc/socket/apic/trunk/parser.c
	$(CC) -c ../../acnuc/socket/apic/trunk/parser.c
md5.o : ../../acnuc/socket/apic/trunk/md5.c
	$(CC) -c ../../acnuc/socket/apic/trunk/md5.c
zsockr.o : ../../acnuc/socket/apic/trunk/zsockr.c
	$(CC) $(CFLAGS) ../../acnuc/socket/apic/trunk/zsockr.c
misc_acnuc.o : ../../acnuc/csrc/trunk/misc_acnuc.c
	$(CC) -c ../../acnuc/csrc/trunk/misc_acnuc.c
dnapars.o : csrc/dnapars.c
	$(CC) -c csrc/dnapars.c
protpars.o : csrc/protpars.c
	$(CC) -c csrc/protpars.c
phylip.o : csrc/phylip.c
	$(CC) -c csrc/phylip.c
seq.o : csrc/seq.c
	$(CC) -c csrc/seq.c
lwl.o : csrc/lwl.c
	$(CC) -c csrc/lwl.c
bionj.o : csrc/bionj.c
	$(CC) -c csrc/bionj.c
phyml_util.o : csrc/phyml_util.c
	$(CC) -c csrc/phyml_util.c

win32: seaview.exe 

seaview.exe: $(OBJECTS) win32_extras.o
	$(CXX)   $(OBJECTS) win32_extras.o -mwindows ../seaview-xcode/seaview.res \
           -L$(FLTK)/lib -lfltk13Win32 -ljpegWin32 -lpngWin32 \
          -L$(PDFLIB) -lpdfQPC   \
	 -L/Users/mgouy/keep/cross/zlib-1.2.1  -lzQPC \
         -lole32 -luuid -lws2_32 -lcomctl32 -o /Users/mgouy/VBoxShared/PC/seaview.exe


.SUFFIXES:	.cxx .h .o

.cxx.o :
	$(CXX) $(CFLAGS) $<
