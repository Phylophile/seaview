#!/bin/csh

echo -n "Version number?"
set V=$<
set tarname=seaview_$V.tar

setenv TMPDIR /tmp/seaview
setenv FROM $PWD
if(-e $TMPDIR) rm -f -r $TMPDIR
mkdir $TMPDIR
cp -p Makefile-distrib $TMPDIR/Makefile
pushd trunk
cp -p addbootstrap.cxx align.cxx comlines.cxx concatenate.cxx custom.cxx least_squares_brl.cxx \
  load_seq.cxx macos_extras.mm nexus.cxx pdf_or_ps.cxx pseudoterminal.cxx \
  racnuc_fetch.cxx regions.cxx resource.cxx seaview.cxx treedraw.cxx trees.cxx unrooted.cxx \
  use_mase_files.cxx viewasprots.cxx win32_extras.cxx xfmatpt.cxx svg.cxx statistics.cxx \
  threads.cxx $TMPDIR
cp -p seaview.h treedraw.h unrooted.h matpt.h svg.h pdf_or_ps.h $TMPDIR
cp -p  seaview.html seaview.1 seaview.1.xml seaview.xcf seaview.xpm seaview.svg ../data\ files/example.nxs ../README $TMPDIR
popd
mkdir $TMPDIR/csrc
pushd trunk/csrc
cp -p bionj.c dnapars.c seq.c phylip.c lwl.c phyml_util.c protpars.c phyml_util.h phylip.h seq.h $TMPDIR/csrc
popd
mkdir $TMPDIR/FL
pushd trunk/FL
cp -p Fl_Native_File_Chooser*.* $TMPDIR/FL
cp -p $HOME/Documents/fltk/fltk-1.3/src/Fl_Native_File_Chooser_common.cxx $TMPDIR/FL
popd
pushd $HOME/Documents/acnuc/socket/apic/trunk
cp -p raa_acnuc.c raa_acnuc.h parser.c parser.h md5.c zsockr.c $TMPDIR/csrc
popd
pushd $HOME/Documents/acnuc/csrc/trunk
cp -p misc_acnuc.c $TMPDIR/csrc
popd
pushd /tmp
tar cfv $tarname seaview
mv $tarname $FROM
popd
gzip $tarname

echo -n "Monter pbil-ftp..."
set rep=$<
set dist=/Volumes/pbil-ftp/mol_phylogeny/seaview
cp -f $tarname.gz $dist/
cp -f $tarname.gz $dist/seaview.tar.gz
cp -f $tarname.gz $dist/archive/
cp -f trunk/seaview.html $dist/
