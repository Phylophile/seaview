#ifdef malloc

#undef strdup
#undef free 
#undef malloc 
#undef calloc
#undef realloc

#include <stdlib.h>
#include <string.h>

#include <stdio.h>
FILE* debugout =NULL;

void *debugmalloc(size_t s)
{
#ifdef DEBUGNAME
	if(debugout==NULL)debugout=fopen("debug.log","w");
#endif
	char *p;
	p = (char *)malloc(s + 16);
	if(p == NULL) return NULL;
	memcpy(p, "SRT1", 4);
	memcpy(p + 4, &s, 4);
	memcpy(p + 8, "SRT2", 4);
	p += 12;
	memcpy(p + s , "END.", 4);
if(debugout!=NULL){fprintf(debugout,"malloc %p\n", p);fflush(debugout);}
	memset(p, '@', s);
	return (void *)p;
}

void *debugcalloc(size_t n, size_t s)
{
	void *p = debugmalloc(n * s);
	if(p != NULL) memset(p, 0, n * s);
	return p;
}


void debugfree(void *p)
{
  if(p==NULL) return;
  if(debugout!=NULL){fprintf(debugout,"free %p\n",p);fflush(debugout);}
	int err = 0, l;
	char *q = ( char *)p - 12;
	if(memcmp(q, "SRT1", 4) != 0) {
		if(debugout!=NULL){fprintf(debugout,"err=1\n");fflush(debugout);}
		err = 1;
		free(p); 
	  return;
	  //exit(1);
		}
	if(memcmp(q+8, "SRT2", 4) != 0) {
		if(debugout!=NULL){fprintf(debugout,"err=2\n");fflush(debugout);}
		err = 2;
		exit(1);
		}
	memcpy(&l, q+4, 4);
	if(memcmp((char *)p+l, "END.", 4) != 0) {
		if(debugout!=NULL){fprintf(debugout,"err=3\n");fflush(debugout);}
		err = 3;
		exit(1);
		}
	memset(q, '@', l + 16);
	free(q);
}


void *debugrealloc(void *p, size_t s)
{
	int l;
	if(p == NULL) return debugmalloc(s);
	void *q = debugmalloc(s);
	if(q == NULL) return NULL;
	memcpy(&l, (char *)p - 8, 4);
	if(l > s) l = s;
	memcpy(q, p, l);
	debugfree(p);
	return q;
}

char *debugstrdup(const char *s)
{
	char *p = (char *)debugmalloc(strlen(s) + 1);
    if(p != NULL) {
      strcpy(p, s);
      if(debugout!=NULL){fprintf(debugout,"debugstrdup %p %s %p\n",s,s,p);fflush(debugout);}
      }
	return p;
}

#endif

