#include <stdlib.h>

/* Usage:
NSLog(CFSTR("entree p=%p"), p);
*/
#ifndef __OBJC__
#define CFSTR(cStr)  ((CFStringRef) __builtin___CFStringMakeConstantString ("" cStr ""))
typedef const struct __CFString *CFStringRef;
#ifdef __cplusplus
extern "C" {
#endif
  void NSLog(CFStringRef format, ...);
#ifdef __cplusplus
  }
#endif
#endif

#ifdef __cplusplus
extern "C" {
#endif
void *debugmalloc(size_t s);
void *debugcalloc(size_t n, size_t s);
void debugfree(void *p);
void *debugrealloc(void *p, size_t s);
char *debugstrdup(const char *s);
#ifdef __cplusplus
	}
#endif
  
#define free debugfree
#define malloc debugmalloc
#define calloc debugcalloc
#define realloc debugrealloc
#ifdef WIN32
#define _strdup debugstrdup
#else
#define strdup debugstrdup
#endif
