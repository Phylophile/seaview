#if defined( linux) && ( defined(__LP64__) || defined(_LP64) )

#include <string.h>

/* avoid calling memcpy that requires new libc version (GLIBC_2.14) not always available in linux64
 put as first object file in link command for 64-bit linux only
 check with:
 objdump -p seaview
 that shoud not mention GLIBC_2.14
 */
void *memcpy(void *dest, const void *src, size_t n)
{
  return memmove(dest, src, n);
}

#endif
